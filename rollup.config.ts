import { dirname, join } from "path";
import nodeResolve from "@rollup/plugin-node-resolve";
import type { Plugin } from "rollup";
import { defineConfig } from "rollup";
import esbuild from "rollup-plugin-esbuild";

const isProd = process.env.NODE_ENV === "production";

export default ["packages/vite-plugin-gjs/plugin", "packages/vite-plugin-gjs/client", "packages/vue-runtime-gtk/runtime"].map((file) =>
  defineConfig({
    input: `${file}.ts`,
    external: (m) => !/\/node_modules\/mitt\//.test(m) && [/\/node_modules\//, /^node:/, /^gi:*/].some(r => r.test(m)),
    output: {
      format: "module",
      sourcemap: !isProd,
      exports: "auto",
      dir: join(dirname(file), 'dist'),
      name: file + ".js",
    },
    plugins: [
      esbuild({ target: "es2021", minify: false }),
      nodeResolve(),
      dynamicImportViteIgnore(),
    ],
  })
);

export function dynamicImportViteIgnore(): Plugin {
  return {
    name: "vitest-internal/dynamic-import",
    renderDynamicImport: () => ({
      left: "import(/* @vite-ignore */ ",
      right: ")",
    }),
  };
}
