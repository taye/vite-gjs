/// <reference types="vite/client" />
/// <reference types="vue/ref-macros" />
/// <reference types="@gi-types/gjs-environment" />
/// <reference types="@gi-types/base-types" />
/// <reference types="@gi-types/gtk4" />

declare module "gi://Gtk" { export * from "@gi-types/gtk4"; }
declare module "gi://Gtk?version=4.0" { export * from "gi://Gtk"; }
declare module "gi://Gdk" { export * from "@gi-types/gdk4"; }
declare module "gi://Gdk?version=4.0" { export * from "gi://Gdk"; }

declare module "*.vue" {
import type { DefineComponent } from "@vue/runtime-core";
  // eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/ban-types
  const component: DefineComponent<{}, {}, any>;
  export default component;
}
