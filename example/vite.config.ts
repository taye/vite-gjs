import {resolve } from "path";
import { defineConfig } from "vite";
import gjs from "vite-plugin-gjs";
import inspect from "vite-plugin-inspect/dist/index.js";

export default defineConfig({
  plugins: [
    gjs({ entry: resolve(__dirname, "main.ts") }),
    gjs.vue(),
    inspect.default(),
  ],
  resolve: {
    alias: {
      // "@": __dirname,
    },
  },
});

