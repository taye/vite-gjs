import { createApp } from "vue-runtime-gtk";

import App from "./App.vue";
import Gtk from 'gi://Gtk?version=4.0'

const { Application, ApplicationWindow, StyleContext, STYLE_PROVIDER_PRIORITY_APPLICATION } = Gtk

const vueApp = createApp(App);
let application = new Application({ application_id: "org.gtk.exampleapp" });

application.connect("activate", () => {
  // @ts-expect-error
  const { __vite__styleProvider: styleProvider } = globalThis
  let win = new ApplicationWindow({ application, title: 'Vue GTK App' });
  StyleContext.add_provider_for_display(win.get_display(), styleProvider, STYLE_PROVIDER_PRIORITY_APPLICATION)

  vueApp.mount(win);
  win.present();
});

// Run the application
application.run([]);
