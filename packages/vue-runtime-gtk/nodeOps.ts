import { markRaw } from "@vue/reactivity";
import { patchProp } from "./patchProp";

const { Adw } = imports.gi;
import type { Label, EventController, Widget } from "gi://Gtk";
import Gtk from "gi://Gtk?version=4.0";
import { debug } from "./utils";

export type TestElement = Widget | EventController;
export type TestText = Label;
export type TestComment = Label;
export type TestNode = TestElement | TestText | TestComment;

const createElement = (tag: string) => {
  debug("CREATE", tag);

  const importParts = tag.split(":");
  const Constructor = importParts.reduce(
    (acc, part) => acc[part],
    imports.gi as any
  );
  const node: TestElement = new Constructor();

  // avoid test nodes from being observed
  markRaw(node);
  return node;
};

const createText = (text: string) => {
  const node = new Gtk.Label({ label: text });

  debug("TEXT", text);

  markRaw(node);
  return node;
};

const createComment = (text: string) => {
  const node = new Gtk.Label({ label: `[vue] ${text}` });
  node.hide();

  debug("COMMENT", text);

  markRaw(node);
  return node;
};

const setText = (node: TestText, text: string) => {
  node.set_text(text);
  debug("SET TEXT", text);
};

const insert = (
  child: TestNode,
  parent: TestElement,
  ref?: TestNode | null
) => {
  debug(
    "INSERT",
    parent.constructor.name,
    "<-",
    child.constructor.name,
    ref?.constructor.name
  );

  if (child instanceof Gtk.EventController) {
    (parent as Widget).add_controller(child);
    return;
  }

  if (parent instanceof Adw.ApplicationWindow) {
    parent.set_content(child);
  } else if (parent instanceof Gtk.Window) {
    parent.set_child(child);
  } else if (parent instanceof Gtk.Widget) {
    child.insert_after(
      parent,
      ref ? ref.get_prev_sibling() : parent.get_last_child()
    );
  } else {
    child.set_parent(parent);
  }
};

const remove = (child: TestNode) => {
  console.info("REMOVE", child.constructor.name);
  child.unparent();
};

const setElementText = (el: TestElement, text: string) => {
  debug("SET ELEMENT TEXT", el.constructor.name, text);
  let lastChild: TestNode;

  while ((lastChild = el.get_last_child())) {
    lastChild.unparent();
  }

  insert(createText(text), el);
};

const parentNode = (node: TestNode) => node.parent;
const nextSibling = (node: TestNode) => node.get_next_sibling();

const querySelector = () => {
  throw new Error("querySelector not supported in test renderer.");
};

const setScopeId = (el: TestElement, id: string) => {
  debug("SET SCOPE ID", el.constructor.name, id);

  if (el instanceof Gtk.Widget)
    el.set_css_classes([...el.get_css_classes(), id]);
};

export const nodeOps = {
  insert,
  remove,
  createElement,
  createText,
  createComment,
  setText,
  setElementText,
  parentNode,
  nextSibling,
  querySelector,
  setScopeId,
  patchProp,
};
