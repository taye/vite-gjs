import { TestElement } from "./nodeOps";
import { hyphenate, isOn } from "@vue/shared";
import Gtk from "gi://Gtk?version=4.0";
import { debug } from "./utils";

const signalMap = new WeakMap<
  TestElement,
  WeakMap<Handler, Record<string, number[]>>
>();
type Handler = (...args: unknown[]) => unknown;

const addSignal = (
  el: TestElement,
  signal: string,
  handler: Handler,
  isUpdate: boolean
) => {
  let signals = signalMap.get(el);

  if (!signals) {
    signals = new WeakMap();
    signalMap.set(el, signals);
  }

  let ids = signals.get(handler);

  if (!ids) {
    ids = {};
    signals.set(handler, ids);
  }

  if (!ids[signal]) ids[signal] = [];

  let wrappedHandler = handler;

  if (isUpdate) {
    const prop = signal.slice(8) as keyof typeof el;
    const initialValue = el[prop];

    // TODO: nextTick() or disable reactivity tracking?
    handler(initialValue);

    wrappedHandler = () => {
      const value = el[prop];
      debug("NOTIFY", el.constructor.name, prop, value);

      return handler(value);
    };
  }

  const handlerId = el.connect(signal, wrappedHandler);
  ids[signal].push(handlerId);
};

const removeSignal = (el: TestElement, signal: string, handler: Handler) => {
  const id = signalMap.get(el)!.get(handler)![signal]!.pop()!;

  el.disconnect(id);
};

export function patchProp(
  el: TestElement,
  key: string,
  prevValue: any,
  nextValue: any
) {
  debug("PATCH", el.constructor.name, key, prevValue, nextValue);

  if (key === "class" && el instanceof Gtk.Widget) {
    el.set_css_classes(nextValue.split(/\s+/));
  } else if (isOn(key)) {
    let event = hyphenate(key.slice(2));
    const isUpdate = event.startsWith("update:");

    if (isUpdate) {
      event = "notify::" + event.slice(7);
    }

    prevValue && removeSignal(el, event, prevValue);
    nextValue && addSignal(el, event, nextValue, isUpdate);
  } else {
    (el as any)[key] = nextValue;
  }
}
