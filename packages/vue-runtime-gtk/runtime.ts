import {
  createRenderer,
  RootRenderFunction,
  CreateAppFunction,
  FunctionalComponent,
  h,
} from "@vue/runtime-core";
import { nodeOps, TestElement } from "./nodeOps";

const { render: baseRender, createApp: baseCreateApp } =
  createRenderer(nodeOps);

export const render: RootRenderFunction<TestElement> = baseRender;
export const createApp: CreateAppFunction<TestElement> = baseCreateApp;

export * from "./nodeOps";
export * from "@vue/runtime-core";

export const resolveComponent =
  (name: string): FunctionalComponent =>
  (props, { attrs, slots }) => {
    return h(name, { ...props, ...attrs }, slots);
  };
