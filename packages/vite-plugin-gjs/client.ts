import WebSocket from "./WebSocket";
import fetch from "./fetch";
import { setTimeout, setInterval } from "./timers";
import Gtk from "gi://Gtk?version=4.0";
import { normalizeSpecifier, prependQuery } from "./utils";
import { RESTART_MESSAGE } from "./constants";

console.log("[vite] connecting...");
// use server configuration, then fallback to inference
const socketProtocol = "ws";
const socketHost = `${"localhost"}:${"3000"}`;
const socket = new WebSocket(`${socketProtocol}://${socketHost}`, "vite-hmr");

function warnFailedFetch(err, path) {
  if (!err.message.match("fetch")) {
    console.error(err);
  }
  console.error(
    `[hmr] Failed to reload ${path}. ` +
      `This could be due to syntax errors or importing non-existent ` +
      `modules. (see errors above)`
  );
}

// Listen for messages
socket.addEventListener("message", async ({ data }) => {
  handleMessage(JSON.parse(data));
});

async function handleMessage(payload: any) {
  switch (payload.type) {
    case "connected":
      console.log(`[vite] connected.`);
      // proxy(nginx, docker) hmr ws maybe caused timeout,
      // so send ping package let ws keep alive.
      setInterval(() => {
        socket.send("ping");
      }, 1000);
      break;
    case "update":
      notifyListeners("vite:beforeUpdate", payload);
      payload.updates.forEach((update: any) => {
        if (update.type === "js-update") {
          queueUpdate(fetchUpdate(update));
        } else {
          // css-update
          const path = update.path.replace(/\?.*/, "");
          console.log(`[vite] css hot updated: ${path}`);
        }
      });
      break;
    case "custom": {
      notifyListeners(payload.event, payload.data);
      break;
    }
    case "full-reload":
      socket.send(RESTART_MESSAGE);
      break;
    case "prune":
      notifyListeners("vite:beforePrune", payload);
      // After an HMR update, some modules are no longer imported on the page
      // but they may have left behind side effects that need to be cleaned up
      // (.e.g style injections)
      // TODO Trigger their dispose callbacks.
      payload.paths.forEach((path: any) => {
        const fn = pruneMap.get(path);
        if (fn) {
          fn(dataMap.get(path));
        }
      });
      break;
    case "error": {
      notifyListeners("vite:error", payload);
      const err = payload.err;
      console.error(
        `[vite] Internal Server Error\n${err.message}\n${err.stack}`
      );
      break;
    }
    default: {
      const check = payload;
      return check;
    }
  }
}
function notifyListeners(event, data) {
  const cbs = customListenersMap.get(event);
  if (cbs) {
    cbs.forEach((cb) => cb(data));
  }
}
let pending = false;
let queued: any[] = [];
/**
 * buffer multiple hot updates triggered by the same src change
 * so that they are invoked in the same order they were sent.
 * (otherwise the order may be inconsistent because of the http request round trip)
 */
async function queueUpdate(p) {
  queued.push(p);
  if (!pending) {
    pending = true;
    await Promise.resolve();
    pending = false;
    const loading = [...queued];
    queued = [];
    (await Promise.all(loading)).forEach((fn) => fn && fn());
  }
}
async function waitForSuccessfulPing(ms = 1000) {
  // eslint-disable-next-line no-constant-condition
  while (true) {
    try {
      await fetch("http://localhost:3000/__vite_ping");
      break;
    } catch (e) {
      await new Promise((resolve) => setTimeout(resolve, ms));
    }
  }
}
// ping server
socket.addEventListener("close", async ({ wasClean }) => {
  if (wasClean) return;
  console.log(`[vite] server connection lost. polling for restart...`);
  await waitForSuccessfulPing();
  socket.send(RESTART_MESSAGE);
});

const sheetsMap = new Map<string, string>();
// @ts-expect-error
const styleProvider = (globalThis.__vite__styleProvider =
  new Gtk.CssProvider());

async function updateStyle(id: string, content: string) {
  sheetsMap.set(id, content);
  loadCss();
}

function removeStyle(id: string) {
  sheetsMap.delete(id);
  loadCss();
}
const loadCss = () =>
  styleProvider.load_from_data(
    Array.from(sheetsMap.values()).join("\n") || "_{}"
  );

async function fetchUpdate({ path, acceptedPath, timestamp }) {
  const mod = hotModulesMap.get(path);
  if (!mod) {
    // In a code-splitting project,
    // it is common that the hot-updating module is not loaded yet.
    // https://github.com/vitejs/vite/issues/721
    return;
  }
  const moduleMap = new Map();
  const isSelfUpdate = path === acceptedPath;
  // make sure we only import each dep once
  const modulesToUpdate = new Set<string>();
  if (isSelfUpdate) {
    // self update - only update self
    modulesToUpdate.add(path);
  } else {
    // dep update
    for (const { deps } of mod.callbacks) {
      deps.forEach((dep) => {
        if (acceptedPath === dep) {
          modulesToUpdate.add(dep);
        }
      });
    }
  }
  // determine the qualified callbacks before we re-import the modules
  const qualifiedCallbacks = mod.callbacks.filter(({ deps }) => {
    return deps.some((dep) => modulesToUpdate.has(dep));
  });
  await Promise.all(
    Array.from(modulesToUpdate).map(async (dep) => {
      const disposer = disposeMap.get(dep);
      if (disposer) await disposer(dataMap.get(dep));
      const importUrl = prependQuery(dep, `t=${timestamp}`);

      try {
        const newMod = await import(normalizeSpecifier(importUrl));
        moduleMap.set(dep, newMod);
      } catch (e) {
        warnFailedFetch(e, dep);
      }
    })
  );
  return () => {
    for (const { deps, fn } of qualifiedCallbacks) {
      fn(deps.map((dep) => moduleMap.get(dep)));
    }
    const loggedPath = isSelfUpdate ? path : `${acceptedPath} via ${path}`;
    console.log(`[vite] hot updated: ${loggedPath}`);
  };
}
const hotModulesMap = new Map();
const disposeMap = new Map();
const pruneMap = new Map();
const dataMap = new Map();
const customListenersMap = new Map();
const ctxToListenersMap = new Map();
// Just infer the return type for now
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
const createHotContext = (ownerPath) => {
  if (!dataMap.has(ownerPath)) {
    dataMap.set(ownerPath, {});
  }
  // when a file is hot updated, a new context is created
  // clear its stale callbacks
  const mod = hotModulesMap.get(ownerPath);
  if (mod) {
    mod.callbacks = [];
  }
  // clear stale custom event listeners
  const staleListeners = ctxToListenersMap.get(ownerPath);
  if (staleListeners) {
    for (const [event, staleFns] of staleListeners) {
      const listeners = customListenersMap.get(event);
      if (listeners) {
        customListenersMap.set(
          event,
          listeners.filter((l) => !staleFns.includes(l))
        );
      }
    }
  }
  const newListeners = new Map();
  ctxToListenersMap.set(ownerPath, newListeners);
  function acceptDeps(deps, callback = () => {}) {
    const mod = hotModulesMap.get(ownerPath) || {
      id: ownerPath,
      callbacks: [],
    };
    mod.callbacks.push({
      deps,
      fn: callback,
    });
    hotModulesMap.set(ownerPath, mod);
  }
  const hot = {
    get data() {
      return dataMap.get(ownerPath);
    },
    accept(deps, callback) {
      if (typeof deps === "function" || !deps) {
        // self-accept: hot.accept(() => {})
        acceptDeps([ownerPath], ([mod]) => deps && deps(mod));
      } else if (typeof deps === "string") {
        // explicit deps
        acceptDeps([deps], ([mod]) => callback && callback(mod));
      } else if (Array.isArray(deps)) {
        acceptDeps(deps, callback);
      } else {
        throw new Error(`invalid hot.accept() usage.`);
      }
    },
    acceptDeps() {
      throw new Error(
        `hot.acceptDeps() is deprecated. ` +
          `Use hot.accept() with the same signature instead.`
      );
    },
    dispose(cb) {
      disposeMap.set(ownerPath, cb);
    },
    prune(cb) {
      pruneMap.set(ownerPath, cb);
    },
    decline() {},
    invalidate() {
      socket.send(RESTART_MESSAGE);
    },
    // custom events
    on: (event, cb) => {
      const addToMap = (map) => {
        const existing = map.get(event) || [];
        existing.push(cb);
        map.set(event, existing);
      };
      addToMap(customListenersMap);
      addToMap(newListeners);
    },
  };
  return hot;
};
/**
 * urls here are dynamic import() urls that couldn't be statically analyzed
 */
function injectQuery(url: string, _queryToInject: string) {
  // skip urls that won't be handled by vite
  if (!url.startsWith(".") && !url.startsWith("/")) {
    return url;
  }

  return url;
}

export { createHotContext, injectQuery, removeStyle, updateStyle };
