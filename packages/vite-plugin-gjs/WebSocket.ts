import mitt from "mitt";

const decoder = new TextDecoder("utf-8");

// TODO: version?
import Soup from "gi://Soup?version=2.4";

export default class WebSocket {
  eventListeners = new WeakMap();
  #connection!: Soup.WebsocketConnection;
  readyState = 0;
  onopen: Function | null = null;
  onclose: Function | null = null;
  onmessage: Function | null = null;
  onerror: Function | null = null;

  #emitter = mitt<Record<"open" | "message" | "close" | "error", any>>();
  addEventListener = this.#emitter.on;
  removeEventListener = this.#emitter.off;

  constructor(url: string, protocols: string | string[] = []) {
    this.#start(url, typeof protocols === "string" ? [protocols] : protocols);
  }

  async #start(url: string, protocols: string[]) {
    try {
      return createConnection({
        url,
        protocols,
        onConnection: this.#onconnection,
      });
    } catch (error) {
      return this.#emitter.emit("error", error);
    }
  }

  #onconnection = (connection: Soup.WebsocketConnection) => {
    this.#connection = connection;
    this.readyState = 1;
    this.#emitter.emit("open");

    connection.connect("message", (_, type, message) => {
      const bytes = message.toArray();
      const data =
        type === Soup.WebsocketDataType.TEXT ? decoder.decode(bytes) : bytes;
      this.#emitter.emit("message", { data });
    });

    connection.connect("error", (_, error) => {
      this.#emitter.emit("error", error);
    });

    connection.connect("closed", () => {
      this.readyState = 3;
      this.#emitter.emit("close");
    });
  };

  send(data: string) {
    this.#connection.send_text(data);
  }

  close() {
    this.readyState = 2;
    this.#connection.close(Soup.WebsocketCloseCode.NORMAL, null);
  }
}

function createConnection({
  url,
  protocols,
  onConnection,
}: {
  url: string;
  protocols: string[];
  onConnection: (connection: Soup.WebsocketConnection) => void;
}) {
  return new Promise<Soup.WebsocketConnection>((resolve, reject) => {
    const session = new Soup.Session();
    const message = new Soup.Message({ method: "GET", uri: Soup.URI.new(url) });
    session.websocket_connect_async(
      message,
      url.replace(/^ws/, "http"),
      protocols,
      null,
      (_session, res) => {
        try {
          const connection = session.websocket_connect_finish(res);
          onConnection(connection);
          resolve(connection);
        } catch (e) {
          reject(e);
        }
      }
    );
  });
}
