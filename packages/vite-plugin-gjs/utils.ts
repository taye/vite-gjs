export const normalizeSpecifier = (name: string) =>
  "./" +
  name
    .replace(/^\/@id\//, "")
    .replace(/_/g, "__")
    .replace(/[?&=\/\\]/g, "_");

export const prependQuery = (url: string, newQuery: string) => {
  const [path, oldQuery] = url.split(`?`);
  return `${path}?${newQuery}${oldQuery ? "&" + oldQuery : ""}`;
};

export function promiseTask<T, R>(
  object: T,
  method: string,
  finish: string,
  ...args: unknown[]
) {
  return new Promise<R>((resolve, reject) => {
    // @ts-expect-error
    object[method](...args, (_self, asyncResult) => {
      try {
        // @ts-expect-error
        resolve(object[finish](asyncResult));
      } catch (err) {
        console.error(err);
        reject(err);
      }
    });
  });
}
