import assert from "node:assert";
import { writeFile } from "node:fs/promises";
import { join } from "node:path";
import MagicString from "magic-string";
import { ViteDevServer } from "vite";
import { CUSTOM_VITE_CLIENT, VITE_PLUGIN_NAME } from "./constants";
import * as lexer from "es-module-lexer";
import { normalizeSpecifier } from "./utils";

export default async function writeModule(
  server: ViteDevServer,
  outDir: string,
  url: string,
  hmrUrl?: string,
  processed = new Set<string>()
) {
  if (processed.has(url)) return processed;
  processed.add(url);

  const [transformResult] = await Promise.all([
    server.transformRequest(url.replace(/^\/@id\//, "")),
    lexer.init,
  ]);

  assert(
    transformResult,
    `[${VITE_PLUGIN_NAME}] failed to transform entry with url ${url}`
  );

  const { code, map } = transformResult;

  const [imports] = lexer.parse(code);
  const s = new MagicString(code);
  const deps: string[] = [];

  for (const imp of imports) {
    let name = imp.n;

    if (!name) continue;
    if (name.startsWith("/@id/gi:")) {
      s.overwrite(imp.s, imp.e, name.replace("/@id/gi:", "gi://"));
      continue;
    }

    if (name === "/@vite/client") {
      s.overwrite(imp.s, imp.e, normalizeSpecifier(CUSTOM_VITE_CLIENT));
      continue;
    }

    s.overwrite(imp.s, imp.e, normalizeSpecifier(name));
    !hmrUrl && deps.push(name);
  }

  const mapUrl = map
    ? "\n//# sourceMappingURL=data:application/json;charset=utf-8;base64," +
      Buffer.from(JSON.stringify(map)).toString("base64")
    : "";
  const outputCode = s.toString() + mapUrl;
  const outPath = join(outDir, normalizeSpecifier(url));
  const hmrOutPath = hmrUrl && join(outDir, normalizeSpecifier(hmrUrl));

  await Promise.all([
    writeFile(outPath, outputCode),
    hmrOutPath ? writeFile(hmrOutPath, outputCode) : null,
    Promise.all(
      deps.map((dep) => writeModule(server, outDir, dep, undefined, processed))
    ),
  ]);

  return processed;
}
