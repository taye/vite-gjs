import { dirname, join, relative, resolve, sep } from "node:path";
import Debug from "debug";
import { Plugin, ViteDevServer } from "vite";
import Vue from "@vitejs/plugin-vue";
import { CompilerOptions } from "@vue/compiler-sfc";
import {
  baseCompile,
  transformModel,
  baseParse,
  ParserOptions,
} from "@vue/compiler-core";
import temp from "temp";
import assert from "node:assert";
import _inspect from "vite-plugin-inspect/dist/index.js";
import { copyFile } from "node:fs/promises";
import { normalizeSpecifier, prependQuery } from "./utils";
import { ChildProcess, spawn } from "node:child_process";
import {
  CUSTOM_VITE_CLIENT,
  GJS_ENTRY_NAME,
  INTERNAL,
  RESTART_MESSAGE,
  VITE_PLUGIN_NAME,
  VUE_RUNTIME_NAME,
} from "./constants";
import { fileURLToPath } from "node:url";
import writeModule from "./writeModule";

temp.track();

const debug = Debug(VITE_PLUGIN_NAME);

export default function gjs({ entry }: { entry: string }): Plugin {
  assert(
    typeof entry === "string",
    `[${VITE_PLUGIN_NAME}] entry option must be a string value`
  );

  let server: ViteDevServer;
  let tempDir: string;
  let gjsEntryModule: string;
  let gjsProcess: ChildProcess | null;

  const plugin: Plugin = {
    name: VITE_PLUGIN_NAME,
    enforce: "post",
    config(config) {
      return {
        build: {
          rollupOptions: {
            input: config.build?.rollupOptions?.input || [entry],
            external: [/^gi:/],
            output: {
              format: "module",
            },
          },
        },
        esbuild: {
          target: "es2021",
        },
        optimizeDeps: {
          // entries: [entry],
        },
      };
    },
    configureServer(_server) {
      server = _server;
      (server as any)[INTERNAL] = plugin;

      server.ws.on("connection", (client) =>
        client.on("message", (message) => {
          if (message.toString() === RESTART_MESSAGE) {
            stopGjs();
            startGjs();
          }
        })
      );
    },
    resolveId(source) {
      if (source.startsWith("gi://")) return source.replace("//", "");
      if (source === "/" + GJS_ENTRY_NAME) return GJS_ENTRY_NAME;
    },
    load(id) {
      if (id === GJS_ENTRY_NAME) {
        const entryUrl = resolve(sep, relative(server.config.root, entry));

        return ["/@vite/env", "/@vite/client", entryUrl]
          .map((i) => `import ${JSON.stringify(i)};`)
          .join("\n");
      }

      return null;
    },
    buildStart() {
      debugger;
      return buildStart();
    },
    async handleHotUpdate(ctx) {
      await Promise.all(
        ctx.modules.map(async (module) => {
          const hmrUrl = prependQuery(module.url, `t=${ctx.timestamp}`);

          await writeModule(server, tempDir, module.url, hmrUrl);
        })
      );
    },
    closeBundle() {
      temp.cleanup();
      stopGjs();
    },
  };

  return plugin;

  async function buildStart() {
    if (!server) return;

    tempDir = await new Promise((resolve, reject) =>
      temp.mkdir(VITE_PLUGIN_NAME, (error: unknown, path: string) =>
        error ? reject(error) : resolve(path)
      )
    );
    (["SIGINT", "SIGTERM"] as const).forEach((s) =>
      process.on(s, () => {
        temp.cleanupSync();
        server.close().catch(() => {});
      })
    );

    const gjsEntryUrl = "/" + GJS_ENTRY_NAME;
    gjsEntryModule = resolve(tempDir, normalizeSpecifier(gjsEntryUrl));

    const [processed] = await Promise.all([
      writeModule(server, tempDir, gjsEntryUrl),
      copyFile(
        join(dirname(fileURLToPath(import.meta.url)), "client.js"),
        join(tempDir, normalizeSpecifier(CUSTOM_VITE_CLIENT))
      ),
    ]);

    debug(`Wrote ${processed.size} modules, starting from ${entry}.`);

    startGjs();
  }
  function startGjs() {
    gjsProcess = spawn("gjs", ["-m", gjsEntryModule!], { stdio: "inherit" });
  }
  function stopGjs() {
    gjsProcess?.kill();
    gjsProcess = null;
  }
}

gjs.vue = () => [
  {
    name: `${VITE_PLUGIN_NAME}:vue`,
    config() {
      return {
        resolve: {
          alias: {
            vue: VUE_RUNTIME_NAME,
          },
        },
      };
    },
  },
  Vue({
    reactivityTransform: "true",
    template: {
      compiler: { compile, parse },
      compilerOptions: {
        runtimeModuleName: VUE_RUNTIME_NAME,
      },
    },
  }),
];

function compile(template: string, options: CompilerOptions = {}) {
  return baseCompile(template, {
    ...options,
    directiveTransforms: {
      ...options.directiveTransforms,
      notify(...args) {
        const res = transformModel(...args);
        return { props: [res.props[1]] };
      },
    },
  });
}

function parse(template: string, options: ParserOptions = {}) {
  return baseParse(template, options);
}
