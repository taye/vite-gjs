export const VITE_PLUGIN_NAME = "vite-plugin-gjs";
export const VUE_RUNTIME_NAME = "vue-runtime-gtk";
export const RESTART_MESSAGE = `${VITE_PLUGIN_NAME}:restart` as const;
export const CUSTOM_VITE_CLIENT = `${VITE_PLUGIN_NAME}:vite-client.js` as const;
export const GJS_ENTRY_NAME = `${VITE_PLUGIN_NAME}:entry.js` as const;
export const INTERNAL = Symbol.for(VITE_PLUGIN_NAME);
